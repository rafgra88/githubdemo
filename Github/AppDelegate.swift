//
//  AppDelegate.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import AlamofireNetworkActivityLogger

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        prepareStartScreen()
        Utils.initializeShowActualizationHUD()
//        #if DEBUG_VERSION
//        NetworkActivityLogger.shared.startLogging()
//        NetworkActivityLogger.shared.level = .debug
//        #endif
        return true
    }

    func prepareStartScreen() {
        window = UIWindow()
        window?.makeKeyAndVisible()
        Navigator.shared.attachWindow(window)
        Navigator.shared.createStartScene()
    }
}
