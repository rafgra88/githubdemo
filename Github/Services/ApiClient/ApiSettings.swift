//
//  ApiSettings.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

final class ApiSettings {
    #if PROD
    static let kApiUrl: String = ""
    #else
    static let kApiUrl: String = "https://api.github.com/"
    #endif
}
