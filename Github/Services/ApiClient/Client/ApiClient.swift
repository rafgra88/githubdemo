//
//  ApiClient.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import Alamofire

final class ApiClient {

    static let shared = {
        return ApiClient()
    }()

    private init() {
    }

    func getUrl(method: ApiMethods, parameter: Int? = nil) -> String {
        var url = ApiSettings.kApiUrl
        url.append(method.rawValue)
        if let parameter = parameter {
            url.append("/")
            url.append(String(parameter))
        }
        return url
    }

    func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        headers: HTTPHeaders? = nil,
        authorization: Bool = true,
        completionHandler: @escaping (AFDataResponse<Any>) -> Void)
        -> DataRequest {
            let headers: HTTPHeaders = ["Accept": "application/vnd.github.v3.full+json"]
            return AF.request(url, method: method,
                                     parameters: parameters,
                                     encoding: URLEncoding.default,
                                     headers: headers).responseJSON(completionHandler: { (response) in
                                        completionHandler(response)
                                     })
    }

    func tryToGetError(response: AFDataResponse<Any>) -> ErrorModel? {
        if let description = response.error?.localizedDescription {
            //cancelled error will be ingnored
            if let error = response.error {
                switch error {
                case .explicitlyCancelled:
                    return ErrorModel(message: description, code: .cancelled)
                default:
                    return ErrorModel(message: description)
                }
            }
        }

        guard let data = response.data else {
            return ErrorModel.defaultError()
        }

        let jsonDecoder = JSONDecoder()
        do {
            let customError = try jsonDecoder.decode(ErrorResponseModel.self, from: data)
            return ErrorModel(message: customError.message)
        } catch {
            return nil
        }
    }
}
