//
//  ApiClient+Search.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation
import Alamofire

extension ApiClient {

    func searchRepositories(query: String,
                            finishBlock: @escaping ([RepositoryModel]?, ErrorModel?) -> Void) -> DataRequest {
        let url = ApiClient.shared.getUrl(method: .searchRepositories)
        let parameters: [String: Any] = [
            "q": query
        ]

        return request(url, method: .get, parameters: parameters) { response in
            if let error = self.tryToGetError(response: response) {
                finishBlock(nil, error)
                return
            }

            let jsonDecoder = JSONDecoder()
            do {
                //data was checked into tryToGetError
                let model = try jsonDecoder.decode(SearchRepositoriesResponseModel.self, from: response.data!)
                finishBlock(model.items, nil)
                return
            } catch {
                finishBlock(nil, ErrorModel.defaultError())
                return
            }
        }
    }
}
