//
//  ApiMethods.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

enum ApiMethods: String {
    case searchRepositories = "search/repositories"
}
