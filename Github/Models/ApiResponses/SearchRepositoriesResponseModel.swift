//
//  SearchRepositoriesResponseModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

final class SearchRepositoriesResponseModel: Codable {
    var items: [RepositoryModel]
}
