//
//  ErrorResponseModle.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

final class ErrorResponseModel: Codable {
    var message: String
    var errors: [ApiErrorModel]?
}
