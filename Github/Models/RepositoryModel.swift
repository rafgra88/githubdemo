//
//  RepositoryModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

final class RepositoryModel: NSObject, Codable {
    var name: String
    var language: String?
    // swiftlint:disable identifier_name
    var stargazers_count: Int
    var owner: OwnerModel
    var size: Int
    var watchers_count: Int
    var open_issues_count: Int
    var forks: Int
    var score: Double
    var default_branch: String
    var has_wiki: Bool
    var has_issues: Bool
    // swiftlint:enable identifier_name
}
