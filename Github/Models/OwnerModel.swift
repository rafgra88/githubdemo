//
//  OwnerModel.swift
//  Github
//
//  Created by Rafal Grabos on 20/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

final class OwnerModel: Codable {
    var id: Int
    var login: String
    // swiftlint:disable identifier_name
    var avatar_url: String
    // swiftlint:enable identifier_name
}
