//
//  ErrorModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

enum ErrorCode: Int, Codable {
    case cancelled
    case unknown
}

final class ErrorModel: Codable {
    var message: String
    var name: String?
    var code: ErrorCode?

    init(message: String, code: ErrorCode = .unknown) {
        self.message = message
        self.code = code
    }

    static func defaultError() -> ErrorModel {
        return ErrorModel.init(message: String(localized: "Error_Unknown"))
    }
}
