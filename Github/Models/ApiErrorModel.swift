//
//  ApiErrorModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

final class ApiErrorModel: Codable {
    var resource: String
    var field: String
    var code: String
}
