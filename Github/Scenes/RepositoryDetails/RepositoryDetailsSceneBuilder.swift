//
//  RepositoryDetailsSceneBuilder.swift
//  Github
//
//  Created by Rafał Graboś on 01/03/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import UIKit

struct RepositoryDetailsSceneBuilder: SceneBuilderProtocol {
    private let item: RepositoryModel

    init(item: RepositoryModel) {
        self.item = item
    }

    func createScene() -> UIViewController {
        let viewModel = RepositoryDetailsViewModel(item: item)
        return RepositoryDetailsViewController(viewModel: viewModel)
    }
}
