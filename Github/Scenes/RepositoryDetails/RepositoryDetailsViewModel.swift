//
//  RepositoryDetailsViewModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

final class RepositoryDetailsViewModel: BaseViewModel {
    private(set) var repository: RepositoryModel? {
        didSet {
            didUpdate?()
        }
    }

    init(item: RepositoryModel) {
        super.init()
        self.repository = item
    }
}
