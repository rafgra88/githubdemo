//
//  RepositoryDetailsViewController.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import Kingfisher

final class RepositoryDetailsViewController: BaseViewController {

    private var repositoryDetailsView: RepositoryDetailsView!
    var viewModel: RepositoryDetailsViewModel

    init(viewModel: RepositoryDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        repositoryDetailsView = RepositoryDetailsView(controllerProtocol: self)
        view = repositoryDetailsView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        repositoryDetailsView.refresh()
        bindToViewModel()
    }

    private func bindToViewModel() {
        viewModel.didUpdate = { [weak self] in
            self?.repositoryDetailsView.refresh()
        }

        viewModel.didError = { [weak self] error in
            self?.showMessage(message: error.message)
        }
    }
}

extension RepositoryDetailsViewController: BaseViewControllerProtocol {

    func getTitle() -> String {
        return viewModel.repository?.name ?? String(localized: "Repository_Details_Title")
    }
}

extension RepositoryDetailsViewController: RepositoryDetailsViewControllerProtocol {

}
