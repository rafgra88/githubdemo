//
//  RepositoryDetailsView.swift
//  Github
//
//  Created by Rafał Graboś on 29/02/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import UIKit

final class RepositoryDetailsView: MainViewControllerView {

    private let margin = 15.0
    private weak var controllerProtocol: RepositoryDetailsViewControllerProtocol?
    private var nameLabel: CommonBoldLabel!
    private var ownerLabel: CommonBoldLabel!
    private var languageLabel: CommonLabel!
    private var starsLabel: CommonLabel!
    private var sizeLabel: CommonLabel!
    private var watchersLabel: CommonLabel!
    private var openIssuesLabel: CommonLabel!
    private var forksLabel: CommonLabel!
    private var scoreLabel: CommonLabel!
    private var defaultBranchLabel: CommonLabel!
    private var hasWikiLabel: CommonLabel!
    private var hasIssuesLabel: CommonLabel!
    private var scrollView: UIScrollView!
    private var contentView: UIView!
    private var userImageView: UIImageView!
    private var stackView: UIStackView!

    init(controllerProtocol: RepositoryDetailsViewControllerProtocol) {
        self.controllerProtocol = controllerProtocol
        super.init(frame: CGRect.zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    private func initialize() {
        initializeScrollView()
        initializeContentView()
        initializeUserImageView()
        initializeStackView()
        initializeLabels()
    }

    private func initializeScrollView() {
        scrollView = UIScrollView()
        addSubview(scrollView)

        scrollView.snp.makeConstraints { (maker) in
            if #available(iOS 11, *) {
                maker.bottom.left.right.top.equalTo(safeAreaLayoutGuide)
            } else {
                maker.bottom.left.right.top.equalToSuperview()
            }
        }
    }

    private func initializeContentView() {
        contentView = UIView()
        scrollView.addSubview(contentView)

        contentView.snp.makeConstraints { (maker) in
            maker.bottom.left.right.top.equalToSuperview()
            maker.width.equalToSuperview()
        }
    }

    private func initializeUserImageView() {
        userImageView = UIImageView()
        contentView.addSubview(userImageView)

        userImageView.snp.makeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(margin)
            maker.width.height.equalTo(100.0)
        }
    }

    private func initializeStackView() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 20.0
        contentView.addSubview(stackView)

        stackView.snp.makeConstraints { (maker) in
            maker.top.equalTo(userImageView.snp.bottom).offset(margin)
            maker.left.equalTo(margin)
            maker.bottom.right.equalTo(-margin)
        }
    }

    private func initializeLabels() {
        nameLabel = CommonBoldLabel()
        ownerLabel = CommonBoldLabel()
        languageLabel = CommonLabel()
        starsLabel = CommonLabel()
        sizeLabel = CommonLabel()
        watchersLabel = CommonLabel()
        openIssuesLabel = CommonLabel()
        forksLabel = CommonLabel()
        scoreLabel = CommonLabel()
        defaultBranchLabel = CommonLabel()
        hasWikiLabel = CommonLabel()
        hasIssuesLabel = CommonLabel()

        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(ownerLabel)
        stackView.addArrangedSubview(languageLabel)
        stackView.addArrangedSubview(starsLabel)
        stackView.addArrangedSubview(sizeLabel)
        stackView.addArrangedSubview(watchersLabel)
        stackView.addArrangedSubview(openIssuesLabel)
        stackView.addArrangedSubview(forksLabel)
        stackView.addArrangedSubview(scoreLabel)
        stackView.addArrangedSubview(defaultBranchLabel)
        stackView.addArrangedSubview(hasWikiLabel)
        stackView.addArrangedSubview(hasIssuesLabel)
    }

    func refresh() {
        guard let repository = controllerProtocol?.viewModel.repository else {
            return
        }

        let url = URL(string: repository.owner.avatar_url)
        userImageView.kf.setImage(with: url)

        nameLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Name",
                                                     postfix: repository.name)
        ownerLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Author",
                                                      postfix: repository.owner.login)

        languageLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Language",
                                                         postfix: repository.language)

        starsLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Stars",
                                                      postfix: String(repository.stargazers_count))

        sizeLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Size",
                                                     postfix: String(repository.size))

        watchersLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Watchers",
                                                         postfix: String(repository.watchers_count))

        openIssuesLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Open_issues",
                                                           postfix: String(repository.open_issues_count))

        forksLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Forks",
                                                      postfix: String(repository.forks))

        scoreLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Score",
                                                      postfix: String(repository.score))

        defaultBranchLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Default_Branch",
                                                              postfix: repository.default_branch)

        hasWikiLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Has_Wiki",
                                                        postfix: (repository.has_wiki.toLocalizedString()))

        hasIssuesLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Has_Issues",
                                                          postfix: repository.has_wiki.toLocalizedString())
    }
}
