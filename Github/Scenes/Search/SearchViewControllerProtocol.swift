//
//  SearchViewControllerProtocol.swift
//  Github
//
//  Created by Rafał Graboś on 29/02/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import Foundation

protocol SearchViewControllerProtocol: NSObjectProtocol {
    var viewModel: SearchViewModel { get }
    func showUnexpectedError()
    func itemSelected(model: RepositoryModel)
}
