//
//  SearchSceneBuilder.swift
//  Github
//
//  Created by Rafał Graboś on 01/03/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import UIKit

struct SearchSceneBuilder: SceneBuilderProtocol {
    func createScene() -> UIViewController {
        let viewModel = SearchViewModel()
        return SearchViewController(viewModel: viewModel)
    }
}
