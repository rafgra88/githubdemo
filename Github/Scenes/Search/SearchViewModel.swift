//
//  SearchViewModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import Alamofire

final class SearchViewModel: BaseViewModel {

    private var currentSearchRequest: DataRequest?
    private var timer: Timer?

    var items: [RepositoryModel]? {
        didSet {
            didUpdate?()
        }
    }

    var searchQuery: String = "" {
        didSet {
            searchQueryChanged()
        }
    }

    private func searchQueryChanged() {
        timer?.invalidate()
        if searchQuery == "" {
            items = nil
            return
        }
        timer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { [weak self] (_) in
            self?.getData(query: self?.searchQuery ?? "")
        }
    }

    private func getData(query: String) {
        currentSearchRequest?.cancel()
        currentSearchRequest = ApiClient.shared.searchRepositories(query: query) { [weak self] (models, error) in
            self?.items = models
            if let error = error, error.code != .cancelled {
                self?.didError?(error)
            }
            self?.currentSearchRequest = nil
        }
    }
}
