//
//  RepositoryTableViewCell.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import SnapKit

final class SearchRepositoryTableViewCell: UITableViewCell {

    static let reuseIdentifier = "RepositoryTableViewCell"

    private let margin = 15.0
    private var nameLabel: CommonBoldLabel!
    private var starsLabel: CommonLabel!
    private var authorLabel: CommonLabel!

    var repository: RepositoryModel? {
        didSet {
            refresh()
        }
    }

    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }

    private func initialize() {
        initializeNameLabel()
        initializeAuthorLabel()
        initializeStarsLabel()
    }

    private func initializeNameLabel() {
        nameLabel = CommonBoldLabel()
        nameLabel.numberOfLines = 0
        contentView.addSubview(nameLabel)

        nameLabel.snp.makeConstraints { (maker) in
            maker.top.left.equalToSuperview().offset(margin)
        }
    }

    private func initializeAuthorLabel() {
        authorLabel = CommonLabel()
        authorLabel.numberOfLines = 0
        contentView.addSubview(authorLabel)

        authorLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(nameLabel.snp.bottom).offset(margin)
            maker.left.equalToSuperview().offset(margin)
            maker.bottom.equalToSuperview().offset(-margin)
            maker.right.equalTo(nameLabel.snp.right)
        }
    }

    private func initializeStarsLabel() {
        starsLabel = CommonLabel()
        contentView.addSubview(starsLabel)

        starsLabel.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().offset(margin)
            maker.right.bottom.equalToSuperview().offset(-margin)
            maker.left.equalTo(nameLabel.snp.right).offset(margin)
            maker.width.equalTo(120.0)
        }
    }

    private func refresh() {
        nameLabel.text = repository?.name
        starsLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Stars",
                                                      postfix: String(repository?.stargazers_count ?? 0))
        authorLabel.text = Utils.mergeStringsWithColon(localizedPrefix: "Author",
                                                       postfix: repository?.owner.login)
    }
}
