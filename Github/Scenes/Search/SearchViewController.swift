//
//  SearchViewController.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

final class SearchViewController: BaseViewController {

    private var searchView: SearchView!
    var viewModel: SearchViewModel

    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        searchView = SearchView(controllerProtocol: self)
        view = searchView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindToViewModel()
    }

    private func bindToViewModel() {
        viewModel.didUpdate = { [weak self] in
            self?.searchView.refresh()
        }

        viewModel.didError = { [weak self] error in
            self?.showMessage(message: error.message)
        }
    }
}

extension SearchViewController: BaseViewControllerProtocol {

    func getTitle() -> String {
        return String(localized: "Search_Title")
    }
}

extension SearchViewController: SearchViewControllerProtocol {

    func itemSelected(model: RepositoryModel) {
        Navigator.shared.goToRepositoryDetails(model: model)
    }
}
