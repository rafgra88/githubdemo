//
//  SearchView.swift
//  Github
//
//  Created by Rafał Graboś on 29/02/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import UIKit

final class SearchView: MainViewControllerView {

    private let margin = 15.0
    private weak var controllerProtocol: SearchViewControllerProtocol?
    private var tableView: UITableView!
    private var searchTextField: CommonTextField!

    init(controllerProtocol: SearchViewControllerProtocol) {
        self.controllerProtocol = controllerProtocol
        super.init(frame: CGRect.zero)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    private func initialize() {
        initializeTextField()
        initializeTableView()
    }

    private func initializeTextField() {
        searchTextField = CommonTextField()
        searchTextField.placeholder = String(localized: "Search_Placeholder")
        searchTextField.addTarget(self, action: #selector(searchTextFieldTextChanged), for: .editingChanged)
        searchTextField.delegate = self
        searchTextField.becomeFirstResponder()
        addSubview(searchTextField)

        searchTextField.snp.makeConstraints { (maker) in
            if #available(iOS 11, *) {
                maker.left.top.equalTo(safeAreaLayoutGuide).offset(margin)
                maker.right.equalTo(safeAreaLayoutGuide).offset(-margin)
            } else {
                maker.left.top.equalToSuperview().offset(margin)
                maker.right.equalToSuperview().offset(margin)
            }
            maker.height.equalTo(30)
        }
    }

    private func initializeTableView() {
        tableView = UITableView()
        tableView.register(SearchRepositoryTableViewCell.self,
                           forCellReuseIdentifier: SearchRepositoryTableViewCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        addSubview(tableView)

        tableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(searchTextField.snp.bottom).offset(margin)
            if #available(iOS 11, *) {
                maker.bottom.left.right.equalTo(safeAreaLayoutGuide)
            } else {
                maker.bottom.left.right.equalToSuperview()
            }
        }
    }

    @objc func searchTextFieldTextChanged() {
        controllerProtocol?.viewModel.searchQuery = searchTextField.text ?? ""
    }

    func refresh() {
        tableView.reloadData()
    }
}

extension SearchView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let arr = controllerProtocol?.viewModel.items, indexPath.row < arr.count else {
            controllerProtocol?.showUnexpectedError()
            return
        }
        let model = arr[indexPath.row]
        controllerProtocol?.itemSelected(model: model)
    }
}

extension SearchView: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllerProtocol?.viewModel.items?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchRepositoryTableViewCell.reuseIdentifier,
                                                       for: indexPath) as? SearchRepositoryTableViewCell else {
            let cell = SearchRepositoryTableViewCell()
            cell.repository = controllerProtocol?.viewModel.items?[indexPath.row]
            return cell
        }

        cell.repository = controllerProtocol?.viewModel.items?[indexPath.row]
        return cell
    }
}

extension SearchView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
