//
//  UIColor+Extension.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UIColor {

    static var commonTextColor: UIColor {
        return UIColor(rgb: 0x1C4F9E)
    }

    static var navigationBarTitleColor: UIColor {
        return UIColor.black
    }

    static var textFieldTextColor: UIColor {
        return UIColor.black
    }

    static var textFieldPlaceholderColor: UIColor {
        return UIColor(rgb: 0x94AAB7)
    }

    static var commonBackgroundColor: UIColor {
        return UIColor(rgb: 0xFAFAFA)
    }

    static var navigationBarBackgroundColor: UIColor {
        return UIColor.white
    }
}
