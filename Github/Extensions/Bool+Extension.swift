//
//  Bool+Extension.swift
//  Github
//
//  Created by Rafal Grabos on 20/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

extension Bool {
    func toLocalizedString() -> String {
        if self {
            return String(localized: "YES")
        }
        return String(localized: "NO")
    }
}
