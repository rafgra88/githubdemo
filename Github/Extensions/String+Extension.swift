//
//  String+Extension.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation

extension String {
    init(localized: String) {
        self = NSLocalizedString(localized, comment: "")
    }
}
