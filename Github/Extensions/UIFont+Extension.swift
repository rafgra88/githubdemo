//
//  UIFont+Extension.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

extension UIFont {

    static var commonLabelFont: UIFont {
        return UIFont.systemFont(ofSize: Constants.FontSize.average)
    }

    static var commonBoldLabelFont: UIFont {
        return UIFont.boldSystemFont(ofSize: Constants.FontSize.average)
    }

    static var commonTextFieldFont: UIFont {
        return UIFont.systemFont(ofSize: Constants.FontSize.average)
    }
}
