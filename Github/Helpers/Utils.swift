//
//  Utils.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import SVProgressHUD

final class Utils {

    static func initializeShowActualizationHUD() {
        SVProgressHUD.setMinimumDismissTimeInterval(TimeInterval.infinity)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setDefaultAnimationType(.native)
    }

    static func showActualization() {
        SVProgressHUD.show()
    }

    static func hideActualization() {
        SVProgressHUD.dismiss()
    }

    static func mergeStringsWithColon(localizedPrefix: String, postfix: String?) -> String {
        return String(localized: localizedPrefix) + ": " + (postfix ?? "")
    }

    static func loadDataFromJson(name: String) -> Data? {
        let bundle = Bundle(for: Utils.self)
        if let path = bundle.url(forResource: name, withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                return data
            } catch {

            }
        }
        return nil
    }
}
