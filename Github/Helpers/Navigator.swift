//
//  Navigator.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import Foundation
import UIKit

final class Navigator {

    private init() {}
    static let shared = {
        return Navigator()
    }()

    private weak var window: UIWindow?
    private var navigationController: NavigationController?

    func attachWindow(_ window: UIWindow?) {
        self.window = window
    }

    func createStartScene() {
        let viewController = SearchSceneBuilder().createScene()
        navigationController = NavigationController(rootViewController: viewController)
        window?.rootViewController = navigationController
    }

    func goToRepositoryDetails(model: RepositoryModel) {
        let viewController = RepositoryDetailsSceneBuilder(item: model).createScene()
        navigationController?.pushViewController(viewController, animated: true)
    }
}
