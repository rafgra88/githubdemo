//
//  SceneBuilderProtocol.swift
//  Github
//
//  Created by Rafał Graboś on 01/03/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import UIKit

protocol SceneBuilderProtocol {
    func createScene() -> UIViewController
}
