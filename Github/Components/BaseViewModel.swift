//
//  BaseViewModel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject {
    // MARK: - Events
    var didError: ((ErrorModel) -> Void)?
    var didUpdate: (() -> Void)?
}
