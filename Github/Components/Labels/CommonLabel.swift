//
//  CommonLabel.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

final class CommonLabel: BaseLabel {

    override func initialize() {
        super.initialize()
        textColor = UIColor.commonTextColor
        font = UIFont.commonLabelFont
        numberOfLines = 0
    }
}
