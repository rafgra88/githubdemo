//
//  CommonBoldLabel.swift
//  Github
//
//  Created by Rafal Grabos on 20/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

final class CommonBoldLabel: BaseLabel {

    override func initialize() {
        super.initialize()
        textColor = UIColor.commonTextColor
        font = UIFont.commonBoldLabelFont
        numberOfLines = 0
    }
}
