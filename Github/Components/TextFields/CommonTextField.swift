//
//  CommonTextField.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit
import SnapKit

final class CommonTextField: UITextField {

    override var placeholder: String? {
        get {
            return attributedPlaceholder?.string
        }
        set {
            guard let string = newValue else {
                attributedPlaceholder = nil
                return
            }
            attributedPlaceholder = NSAttributedString(string: string, attributes: [
                NSAttributedString.Key.font: UIFont.commonTextFieldFont,
                NSAttributedString.Key.foregroundColor: UIColor.textFieldPlaceholderColor
                ])
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    func initialize() {
        textColor = UIColor.textFieldTextColor
        font = UIFont.commonTextFieldFont
        borderStyle = .roundedRect
    }
}
