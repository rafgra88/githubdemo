//
//  MainViewControllerView.swift
//  Github
//
//  Created by Rafał Graboś on 01/03/2020.
//  Copyright © 2020 RG. All rights reserved.
//

import UIKit

class MainViewControllerView: UIView {

    deinit {
        debugPrint("deinit " + String(describing: self))
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.commonBackgroundColor
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.commonBackgroundColor
    }
}
