//
//  BaseViewController.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

protocol BaseViewControllerProtocol {
    func getTitle() -> String
}

class BaseViewController: UIViewController {

    deinit {
        debugPrint("deinit " + String(describing: self))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let base = self as? BaseViewControllerProtocol {
            navigationItem.title = base.getTitle()
        }
    }

    func showUnexpectedError() {
        showMessage(message: ErrorModel.defaultError().message)
    }

    func showMessage(message: String, dismissAction: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: dismissAction))
        present(alert, animated: true, completion: nil)
    }

    func showMessageIfNotNil(message: String?) {
        if let message = message {
            showMessage(message: message, dismissAction: nil)
        }
    }

    func showMessageIfNotNil(message: String?, dismissAction: ((UIAlertAction) -> Void)? = nil) {
        if let message = message {
            showMessage(message: message, dismissAction: dismissAction)
        }
    }

    func startLoading() {
        Utils.showActualization()
    }

    func stopLoading() {
        Utils.hideActualization()
    }

    func dismiss() {
        navigationController?.popViewController(animated: true)
    }
}
