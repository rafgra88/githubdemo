//
//  Constants.swift
//  Github
//
//  Created by Rafal Grabos on 19/04/2019.
//  Copyright © 2019 RG. All rights reserved.
//

import UIKit

struct Constants {

    struct Controls {
        static let commonCornerRadius: CGFloat = 4.0
    }

    struct FontSize {
        static let small: CGFloat = 13.0
        static let average: CGFloat = 15.0
        static let big: CGFloat = 17.0
    }
}
