//
//  AppDelegate.swift
//  Github
//
//  Copyright © 2020 RG. All rights reserved.
//

import XCTest

final class ModelsCase: XCTestCase {

    func testSearchAlamofireResponseModel() {
        if let data = Utils.loadDataFromJson(name: "SearchAlamofire") {
            let jsonDecoder = JSONDecoder()
            do {
                let model = try jsonDecoder.decode(SearchRepositoriesResponseModel.self, from: data)
                XCTAssert(model.items.count == 30)
                XCTAssert(model.items.first?.name == "Alamofire")
                XCTAssertNotNil(model)
                return
            } catch {
                XCTAssert(false)
            }
        }
        XCTAssert(false)
    }

    func testEmptySearchResponseModel() {
        if let data = Utils.loadDataFromJson(name: "SearchEmpty") {
            let jsonDecoder = JSONDecoder()
            do {
                let model = try jsonDecoder.decode(SearchRepositoriesResponseModel.self, from: data)
                XCTAssert(model.items.count == 0)
                XCTAssertNotNil(model)
                return
            } catch {
                XCTAssert(false)
            }
        }
        XCTAssert(false)
    }
}
